#!/usr/bin/env python3
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>
# Adapted from https://betterprogramming.pub/python-shorts-artificial-gravity-in-space-3c2146eff150

import argparse
from math import pi


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Calculate artificial gravity requirements",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    return parser.parse_args()


def main():
    """Onward!"""
    args = get_args()
    purpose = (
        "\nCalculate factors to generate x G of artificial gravity in space"
    )

    print(purpose + "\n" + "-" * len(purpose))
    print("The three factors are:")
    print("* spin rate (seconds per rotation)")
    print("* radius of rotation (meters)")
    print("* G forces created at the circumference (normal gravity is 1.0)")

    while True:
        artificial_gravity()
        repeat = input("\nRun again? [Yn] ")
        if not repeat or repeat.lower().startswith("y"):
            continue
        break


def artificial_gravity():
    """Calculate factors to generate x G of artificial gravity in space"""
    radius = "Radius (m): "
    spin = "Seconds/rotation: "
    gforce = "Gs of acceleration: "

    print("\nEnter any two knowns ...\n")
    s = input(spin)
    s = float(s) if s else 0
    r = input(radius)
    r = float(r) if r else 0
    g = input(gforce)
    g = float(g) if g else 0
    if not r:
        r = 9.8 * g * s**2 / (4 * pi**2)
    elif not s:
        s = 2 * pi / (9.8 * g / r) ** 0.5
    elif not g:
        g = 4 * pi * pi * r / 9.8 / s**2
    print()
    print(spin, s)
    print(radius, r)
    print(gforce, g)


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
