#!/usr/bin/env python3
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

import argparse
from pathlib import Path
import time
from exif import Image


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description=(
            "Rename photo(s) using datetime from EXIF metadata in format "
            "'YYYYMMDD_HHMMSS.jpg'. Photo names beginning with an underscore "
            "are excluded."
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "file",
        metavar="FILE",
        type=str,
        nargs="+",
        help="file(s) or folder(s)",
    )
    parser.add_argument(
        "-d",
        "--dryrun",
        action="store_true",
        help="perform a trial run with no changes made",
    )

    return parser.parse_args()


def main():
    """Onward!"""
    arg = get_args()
    start_time = time.time()
    file_counter, rename_counter = 0, 0
    album = photo_search(arg.file)

    if arg.dryrun:
        print("\n## RENAMED PHOTOS (dryrun) ##\n")
    else:
        print("\n## RENAMED PHOTOS ##\n")
    if album:
        for photo in album:
            file_counter += 1
            datetime = exif_datetime(photo)
            renamed = rename_photo(photo, datetime, arg.dryrun)
            if renamed:
                rename_counter += 1
    end_time = time.time()
    print(
        f"\nProcessed: {file_counter} files\tRenamed: {rename_counter} files"
        f"\tElapsed time: {round(end_time - start_time, 2)} seconds"
    )


def photo_search(filepath):
    """Return list of photos found in filepath."""
    suffix = (".jpg", ".jpeg")
    photos = []

    for p in filepath:
        path = Path(p)
        if path.exists():
            if path.is_dir():
                photos = [
                    photo
                    for photo in path.rglob("*")
                    if photo.suffix.lower() in suffix
                ]
            elif path.is_file():
                if path.suffix.lower() in suffix:
                    photos.append(path)
        else:
            return None

    # Basenames that begin with an underscore are excluded:
    for photo in photos:
        if photo.name.startswith("_"):
            photos.remove(photo)

    return photos


def exif_datetime(photo):
    """Return EXIF datetime_original from photo."""
    with open(photo, "rb") as f:
        image = Image(f)
    # Note: list EXIF attributes using the list_all() method.
    if image.has_exif:
        try:
            # Get the original datetime the image was photographed:
            datetime = image.get("datetime_original")
            mod_date = datetime.replace(" ", "_").replace(":", "")
        except Exception as error:
            print(f"ERROR: {photo}: {type(error).__name__} {error}")
            return None
        else:
            return mod_date
    else:
        return None


def rename_photo(photo, datetime, dryrun):
    """Rename photo to new_name."""
    renamed = False

    if datetime:
        old_name = Path(photo)
        jpg = f"{datetime}.jpg"
        # If photo name already in desired format, do nothing:
        if old_name.name == jpg:
            pass
        else:
            new_name = old_name.with_name(jpg)
            if dryrun:
                pass
            else:
                old_name.rename(new_name)
            renamed = True
            print(f"{photo} --> {new_name}")

    return renamed


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
