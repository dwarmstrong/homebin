#!/usr/bin/env python3
#
# new.py -- by Daniel Wayne Armstrong <hello@dwarmstrong.org>

PURPOSE = ""

import argparse


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Python script template",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "positional",
        metavar="str",
        help="A positional argument",
    )

    return parser.parse_args()


def main():
    """Onward!"""
    args = get_args()


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
