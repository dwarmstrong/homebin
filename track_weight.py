#!/usr/bin/env python3
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

import argparse
from pathlib import Path
import re
import tatt

TITLE = "Track Weight"
LABEL = "kg"
PATH = Path.home().joinpath("doc", "wiki", "index.wiki")
# use parentheses to create groups in the regex
PATTERN = re.compile(r"(\d\d\d\d-\d\d-\d\d).+\s:Weight:\s(\d\d\.\d)")


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Match dates with corresponding weight measurements",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "-r",
        "--results",
        metavar="FORMAT",
        type=str,
        choices=["columns", "dictionary", "heatmap", "scatterplot", "summary"],
        default="summary",
        help=(
            "Display results in FORMAT: columns, dictionary, heatmap, "
            "scatterplot, summary"
        ),
    )
    parser.add_argument(
        "-s",
        "--save",
        metavar="FILE",
        type=str,
        help="Save matches to FILE",
    )

    return parser.parse_args()


def main():
    """Onward!"""
    args = get_args()
    dates_values = tatt.results_dict(PATH, PATTERN)

    if args.save:
        print(tatt.save_data(dates_values, args.save))
    elif args.results == "dictionary":
        print(dates_values)
    elif args.results == "scatterplot":
        tatt.results_scatterplot(dates_values, TITLE, LABEL)
    elif args.results in ["columns", "heatmap"]:
        dframe = tatt.convert_dict_to_dframe(dates_values, LABEL)
        if args.results == "columns":
            print(dframe)
        elif args.results == "heatmap":
            layout = {"height": 4500}
            tatt.results_heatmap(
                dframe, TITLE, LABEL, layout=True, update_layout=layout
            )
    elif args.results == "summary":
        summary = tatt.results_summary(
            dates_values, round_num=True, num_digits=1
        )
        for line in summary:
            print(line)


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
