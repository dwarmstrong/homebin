#!/usr/bin/env python3
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

import argparse
from pathlib import Path
import re
import tatt

TITLE = "Track Number of Steps"
LABEL = "Steps"
PATH = Path.home().joinpath("doc", "wiki", "index.wiki")
# use parentheses to create groups in the regex
PATTERN = re.compile(r"(\d\d\d\d-\d\d-\d\d).+\s:Steps:\s(\d+)")
TARGET = 10000      # daily target for # of steps


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Match dates with corresponding number of steps",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "-r",
        "--results",
        metavar="FORMAT",
        type=str,
        choices=["columns", "dictionary", "heatmap", "summary"],
        default="summary",
        help=(
            "Display results in FORMAT: columns, dictionary, heatmap, summary"
        ),
    )
    parser.add_argument(
        "-s",
        "--save",
        metavar="FILE",
        type=str,
        help="Save matches to FILE",
    )

    return parser.parse_args()


def main():
    """Onward!"""
    args = get_args()
    dates_values = tatt.results_dict(PATH, PATTERN)

    if args.save:
        print(tatt.save_data(dates_values, args.save))
    elif args.results == "dictionary":
        print(dates_values)
    elif args.results in ["columns", "heatmap"]:
        dframe = tatt.convert_dict_to_dframe(dates_values, LABEL)
        if args.results == "columns":
            print(dframe)
        elif args.results == "heatmap":
            tatt.results_heatmap(dframe, TITLE, LABEL)
    elif args.results == "summary":
        summary = tatt.results_summary(dates_values, round_num=True)
        for line in summary:
            print(line)
        target_days = 0
        for value in dates_values.values():
            if value >= TARGET:
                target_days += 1
        print(f"Target: {target_days}\t(# of days with {TARGET}+ steps)")


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
