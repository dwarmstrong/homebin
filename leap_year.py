#!/usr/bin/env python3
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

import argparse


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description=(
            "Determine if YEAR is a leap year, and when the next leap year "
            "after YEAR will be."
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "year",
        metavar="YEAR",
        type=int,
        help="any year",
    )

    return parser.parse_args()


def main():
    """Onward!"""
    args = get_args()
    is_leap_year, next_leap_year = leap_years(args.year)

    print(f"Leap year: {is_leap_year}")
    print(f"The next leap year after {args.year} is {next_leap_year}.")


def leap_years(year):
    """
    Determine if year is a leap year, and when the next leap year will be.
    Return a tuple of (True|False, next_leap).
    """
    leap = is_leap_year(year)
    next_leap = year

    while True:
        next_leap += 1
        y = is_leap_year(next_leap)
        if y:
            break

    return leap, next_leap


def is_leap_year(year):
    """Determine if year is a leap year. Return True or False."""
    # Rules:
    # 1. If year is evenly divisble by 4, its a leap year (2024)...
    # 2. ...UNLESS its evenly divisible by 100, then not a leap year (2100)...
    # 3. ...UNLESS its evenly divisible by 400, then its a leap year (2400)
    if year % 4 == 0:
        is_leap_year = True
        if year % 100 == 0:
            is_leap_year = False
        if year % 400 == 0:
            is_leap_year = True
    else:
        is_leap_year = False

    return is_leap_year


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
