#!/usr/bin/env python3
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

import argparse
from pathlib import Path
import re
import tatt

TITLE = "Track Daily Math Sessions"
LABEL = "Sessions"
PATH = Path.home().joinpath("doc", "wiki", "index.wiki")
# use parentheses to create groups in the regex
PATTERN = re.compile(r"(\d\d\d\d-\d\d-\d\d).+\s(:xDaysOfMath:)")


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Match dates with corresponding math sessions",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "-r",
        "--results",
        metavar="FORMAT",
        type=str,
        choices=["columns", "dictionary", "heatmap", "summary"],
        default="summary",
        help=(
            "Display results in FORMAT: columns, dictionary, heatmap, summary"
        ),
    )
    parser.add_argument(
        "-s",
        "--save",
        metavar="FILE",
        type=str,
        help="Save matches to FILE",
    )

    return parser.parse_args()


def main():
    """Onward!"""
    args = get_args()
    dates_values = tatt.results_dict(PATH, PATTERN)

    if args.save:
        print(tatt.save_data(dates_values, args.save))
    elif args.results == "dictionary":
        print(dates_values)
    elif args.results in ["columns", "heatmap", "summary"]:
        dframe = tatt.convert_dict_to_dframe(dates_values, LABEL)
        if args.results == "columns":
            print(dframe)
        elif args.results == "heatmap":
            dframe2 = tatt.convert_str_to_int(dframe, ":xDaysOfMath:")
            tatt.results_heatmap(dframe2, TITLE, LABEL, scale=False)
        elif args.results == "summary":
            summary = tatt.results_summary_streak(dframe)
            for line in summary:
                print(line)


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
