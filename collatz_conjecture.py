#!/usr/bin/env python3
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

import argparse
import sys


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description=(
            'Explore the Collatz conjecture, the "simplest impossible math '
            'problem"'
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "integer",
        type=int,
        nargs="?",
        help="a positive integer",
    )

    return parser.parse_args()


def main():
    """Onward!"""
    args = get_args()

    if args.integer is None:
        try:
            start = int(input("Enter a positive integer: "))
        except ValueError:
            print("error: invalid int value")
            sys.exit(1)
        start = test_int(start)
    else:
        start = test_int(args.integer)

    sequence, max_num, count = collatz(start)
    print(
        f"\nSequence:\n{sequence}\n\nMaximum number:\n{max_num}\n\nNumber of "
        f"steps:\n{count}"
    )


def test_int(number):
    """
    Test if number is a positive integer. Return number if True or error
    message if False.
    """
    if isinstance(number, int) and number >= 1:
        return number
    else:
        print(f"error: invalid int value: '{number}'")
        sys.exit(1)


def collatz(number):
    """
    Perform arithmetic operations to transform any positive_integer into 1.
    Return the sequence of numbers involved, highest number, and the number of
    steps to reach 1.
    """
    sequence = [number]
    max_num, count = 0, 0

    while sequence[-1] > 1:
        current_num = sequence[-1]

        # For any arbitrary integer:
        # * if the number is even, divide it by two
        # * if the number is odd, triple it and add one
        if current_num % 2 == 0:
            sequence.append(current_num // 2)
        else:
            sequence.append(current_num * 3 + 1)
        count += 1
    max_num = max(sequence)

    return sequence, max_num, count


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
