# homebin

Small programs in my `~/bin`.

## AUTHOR

[Daniel Wayne Armstrong](https://www.dwarmstrong.org)

## LICENSE

GPLv3. See [LICENSE](https://gitlab.com/dwarmstrong/homebin/-/blob/master/LICENSE.md) for more details.
