#!/usr/bin/env bash
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

set -euo pipefail

SCRIPT=$(basename $0)
PURPOSE="Track all the things"


Help() {
  echo "usage: $SCRIPT [OPTION]"
  echo ""
  echo "$PURPOSE"
  echo ""
  echo "options:"
  echo "  -h    show this help message and exit"
  echo ""
}


run_options() {
  while getopts ":h" OPT; do
    case $OPT in
      h)  Help
          exit;;
      \?) echo "error: Invalid option"
          exit;;
    esac
  done
}


load_libraries() {
  # add useful functions
  local lib="lib_homebin.sh"
  local bin="${HOME}/bin/${lib}"
  local src="https://gitlab.com/dwarmstrong/homebin/-/blob/master/${lib}"

  if [ -f $bin ]; then
    source $bin
  else
    echo "error: $bin not found"
    echo "source: $src"
    exit 1
  fi
}


main() {
  banner "Track Morning Rise Time"
  track_rise.py

  banner "Track Pushups"
  track_pushups.py
  track_pushups.py -r heatmap

  banner "Track Daily Walks"
  track_walks.py
  track_walks.py -r heatmap

  banner "Track Steps"
  track_steps.py
  track_steps.py -r heatmap

  banner "Track Daily Math Sessions"
  track_math.py
  track_math.py -r heatmap

  banner "Track Weight"
  track_weight.py
  track_weight.py -r scatterplot
}


# (O<  Let's go!
# (/)_
run_options "$@"
load_libraries
main
