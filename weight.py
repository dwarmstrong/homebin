#!/usr/bin/env python3
#
# weight.py -- by Daniel Wayne Armstrong <hello@dwarmstrong.org>

PURPOSE = "Convert lbs<->kg and calculate BMI"

def main():
    """Onward!"""
    height = 1.72
    
    print(PURPOSE)
    print("-" * len(PURPOSE))

    while True:
        units = input("Units of measurement: lbs [1] or kg [2]? ")
        if units == "1":
            weight = float(input("Enter weight in lbs: "))
            weight = round(weight * 0.453592, 1)
            print(f"Weight in kg: {weight}")
            break
        elif units == "2":
            weight = float(input("Enter weight in kg: "))
            print(f"Weight in lbs: {round(weight * 2.20462, 1)}")
            break
        else:
            print("Enter 1 or 2")

    bmi, status = calculate_bmi(height, weight)
    print(f"BMI: {bmi} ({status})")


def calculate_bmi(height, weight):
    """Return BMI and status"""
    bmi = round(weight / height**2, 1)

    if bmi < 18.5:
        status = "Underweight"
    elif 18.4 < bmi < 25:
        status = "Normal"
    elif 24.9 < bmi < 30:
        status = "Overweight"
    else:
        status = "Obese"

    return bmi, status


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
