#!/usr/bin/env bash
#
# blockheads -- by Daniel Wayne Armstrong <hello@dwarmstrong.org>

set -euo pipefail

SCRIPT=$(basename $0)
PURPOSE="Search for IP addresses blocked by UFW (Uncomplicated Firewall)"

address_only=0
find_all=1
range="today"


Help() {
  echo "usage: $SCRIPT [-a] [-h] [-S DATE]"
  echo ""
  echo "$PURPOSE"
  echo ""
  echo "options:"
  echo "  -a	show addresses only"
  echo "  -h	show this help message and exit"
  echo "  -S \"DATE\"	show entries on or newer than the specified DATE"
}


run_options() {
  while getopts ":ahS:" OPT; do
    case $OPT in
      a)
        address_only=1
        ;;
      h)
        Help
        exit
        ;;
      S)
        range="$OPTARG"
        find_all=0
	      ;;
      ?)
        echo "\"$OPTARG\": Invalid option"
        exit 1
	      ;;
    esac
  done
}


find_blocks() {
  cmd="journalctl"
  out="-o short-iso"
  s="UFW BLOCK"
  if ! command -v -- "$cmd" > /dev/null 2>&1; then
    echo "$cmd: Command not found"
    exit 1
  fi
  if [ $find_all == 1 ]; then
    if [ $address_only == 1 ]; then
      $cmd $out | grep "${s}" | cut -d " " -f 1,9 | cut -d "=" -f 2 | sort -u
    else
      $cmd $out | grep "${s}" | cut -d " " -f 1,9
    fi
  else
    if [ $address_only == 1 ]; then
      $cmd --since "${range}" $out | grep "${s}" | cut -d " " -f 1,9 | \
        cut -d "=" -f 2 | sort -u
    else
      $cmd --since "${range}" $out | grep "${s}" | cut -d " " -f 1,9
    fi
  fi  
}


main() {
  find_blocks
}


# (O<  Let's go!
# (/)_
run_options "$@"
main
