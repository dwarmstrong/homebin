#!/usr/bin/env python3
#
# whereis_earth.py -- Daniel Wayne Armstrong <hello@dwarmstrong.org>

import argparse
from datetime import datetime
import math
from pathlib import Path
import spiceypy


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Find the position of the Earth using SPICE + Spiceypy",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    return parser.parse_args()


def main():
    """Onward!"""
    # Adapted from "Space Science with Python - Part 4: The Earth":
    #   https://www.youtube.com/watch?v=Rq1IeBX44_M
    args = get_args()
    path = Path.home().joinpath("code", "spice", "kernels")
    kernels = [
        f'{Path.joinpath(path, "lsk", "naif0012.tls")}',
        f'{Path.joinpath(path, "spk", "planets", "de432s.bsp")}',
    ]

    # SPICE:
    #   https://naif.jpl.nasa.gov/naif/toolkit.html
    # An observation geometry system for space science missions developed by NASA, comprising a
    # library and toolkit:
    #   https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/index.html

    # Though SPICE itself is not Python, we can use SPICE with the Pythonic wrapper `spiceypy` to
    # start computing Earth's location and velocity:
    #   https://pypi.org/project/spiceypy/

    # SPICE stores data for its repository of space objects in various kernels.
    # The generic kernels directory:
    #   https://naif.jpl.nasa.gov/pub/naif/generic_kernels
    # ...holds data stores not tied to a specific mission.
    # Required kernels for this script are found in the `lsk` and `spk` subdirectories:
    # * `lsk` holds the kernel to handle leapseconds for _ephemeris time_; used in almost any
    # SPICE-based computation
    # * `spk` kernels are for planets, natural satellites, a few asteroids and comets, and
    # Deep Space Network (DSN) ground stations

    # Load kernels:
    spiceypy.furnsh(kernels)

    # Get today's date, convert to str, replacing the time with midnight:
    date_today = datetime.today().strftime("%Y-%m-%dT00:00:00")
    print(f"Today's date (midnight): {date_today}")
    # SPICE doesn't work with UTC time; convert to Ephemeris Time:
    et_date_today = spiceypy.utc2et(date_today)
    print(f"Ephemeris Time (midnight): {et_date_today}")

    # Compute the state vector of the Earth with regard to the Sun.
    # * Target `targ=` is the Earth barycenter, and its NAIF Integer ID is 399. Source:
    #   https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/naif_ids.html
    # * `ECLIPJ2000` is the ecliptic plane
    # * Observer is the Sun, and its NAIF ID is 10:
    earth_state_wrt_sun, earth_sun_light_time = spiceypy.spkgeo(
        targ=399, et=et_date_today, ref="ECLIPJ2000", obs=10
    )

    # The state vector is 6 dimensional: x,y,z in km and the corresponding velocities in km/s:
    print(
        "\nState vector of Earth with regards to the Sun today (midnight): "
        f"\n{earth_state_wrt_sun}"
    )

    # Distance should be around 1 astronomical unit (AU). First, we compute the distance in km:
    earth_sun_distance_km = math.sqrt(
        earth_state_wrt_sun[0] ** 2.0
        + earth_state_wrt_sun[1] ** 2.0
        + earth_state_wrt_sun[2] ** 2.0
    )
    # Convert the distance into AU:
    earth_sun_distance_au = spiceypy.convrt(earth_sun_distance_km, "km", "AU")

    # Display current distance:
    print("\nCurrent distance between the Earth and the Sun in:")
    print(f"* km: {earth_sun_distance_km}")
    print(f"* AU: {earth_sun_distance_au}")


# (O<  Let's go!
# (/)_
if __name__ == "__main__":
    main()
