# Name   : [T]rack [A]ll [T]he [T]hings
# Purpose: Collection of functions for searching files for date+value regex
# Author : Daniel Wayne Armstrong <hello@dwarmstrong.org>

import datetime
import json
from pathlib import Path
import re
import pandas as pd
import plotly.express as px
from plotly_calplot import calplot
import tatt


def convert_minutes_to_time(minutes):
    """Convert minutes (int) and return a tuple (hours, minutes)."""
    hrs = minutes // 60
    mins = minutes % 60

    return (hrs, mins)


def convert_str_to_int(dataframe, string):
    """
    Take a pandas dataframe and replace all instances of string with
    int_value = 1. Return the modified dataframe.
    """
    # FutureWarning: Downcasting behavior in `replace` is deprecated and will
    # be removed in a future version. To retain the old behavior, explicitly
    # call `result.infer_objects(copy=False)`. To opt-in to the future
    # behavior, set `pd.set_option('future.no_silent_downcasting', True)`.
    pd.set_option("future.no_silent_downcasting", True)
    df2 = dataframe.replace(string, 1)

    return df2


def convert_time_to_minutes(time_str):
    """Convert time_str of [h]h:mm and return minutes (int)."""
    hr_mm = time_str.split(":")
    minutes = int(hr_mm[0]) * 60 + int(hr_mm[1])

    return minutes


def convert_dict_to_dframe(dictionary, label):
    """
    Convert dictionary of dates and values and return pandas dataframe of
    'Date' and label columns.
    """
    dates_values = dictionary
    dframe = pd.DataFrame(list(dates_values.items()), columns=["Date", label])
    # convert the 'Date' column to datetime format
    dframe["Date"] = pd.to_datetime(dframe["Date"])
    pd.set_option("display.max_rows", None)

    return dframe


def results_dict(path, pattern):
    """
    Search contents of file located at path for matching pattern of dates and
    values. Return dictionary of dates and values as key-value pairs.
    """
    content = path.read_text(encoding="utf-8")
    matches = pattern.finditer(content)
    dates_values = {}

    for match in matches:
        date = match.groups()[0]
        v = match.groups()[1]
        try:
            value = float(v)
        except ValueError:
            value = v
        dates_values[date] = value

    return dates_values


def results_heatmap(
    dataframe,
    map_title,
    y_axis,
    dark=True,
    color="greens",
    scale=True,
    layout=False,
    update_layout=None,
):
    """
    Take a pandas dataframe with date and value columns and return an
    interactive calendar heatmap.
    """
    fig = calplot(
        dataframe,
        x="Date",
        y=y_axis,
        title=map_title,
        years_title=True,
        showscale=scale,
        dark_theme=dark,
        gap=2,
        colorscale=color,
        # space_between_plots=0.2,
        month_lines_width=2,
    )
    # Python Figure Reference: layout
    # https://plotly.com/python/reference/layout/
    if layout:
        fig.update_layout(update_layout)
    fig.show()


def results_scatterplot(dictionary, title, label="Values"):
    """
    Take a dictionary of dates and values as key-value pairs. Return a scatter
    plot graph with title and label for dates (x-axis) and values (y-axis).
    """
    x_dates = list(dictionary.keys())
    y_values = list(dictionary.values())
    x_dates.reverse()
    y_values.reverse()
    fig = px.scatter(
        x=x_dates,
        y=y_values,
        title=title,
        labels={"x": "Dates", "y": label},
    )
    fig.show()


def results_summary(dictionary, round_num=False, num_digits=None):
    """
    Scan dictionary of dates and values as key-value pairs and return a
    summary of the data.
    """
    dates_values = dictionary
    for key, value in dates_values.items():
        dates_values[key] = float(value)
    entries = len(dates_values)
    latest = next(iter(dates_values.items()))  # tuple with (date, value)
    high = (
        max(dates_values, key=dates_values.get),
        max(dates_values.values()),
    )
    low = (min(dates_values, key=dates_values.get), min(dates_values.values()))
    mean = sum(dates_values.values()) / entries
    if round_num:
        summary = [
            f"Latest: {round(latest[1], ndigits=num_digits)}\t(recorded on "
            f"{latest[0]})",
            f"High  : {round(high[1], ndigits=num_digits)}\t(recorded on "
            f"{high[0]})",
            f"Low   : {round(low[1], ndigits=num_digits)}\t(recorded on "
            f"{low[0]})",
            f"Mean  : {round(mean, ndigits=num_digits)}\t(calculated from "
            f"{entries} items)",
        ]
    else:
        summary = [
            f"Latest: {latest[1]}\t(recorded on {latest[0]})",
            f"High  : {high[1]}\t(recorded on {high[0]})",
            f"Low   : {low[1]}\t(recorded on {low[0]})",
            f"Mean  : {mean}\t(calculated from {entries} items)",
        ]

    return summary


def results_summary_streak(dataframe):
    """
    Take a pandas dataframe with date and value columns and return a list of
    date statistics.
    """
    df = dataframe
    # Retrieve the last (earliest) timestamp from dataframe's 'Date' column
    start_date = pd.Timestamp(df["Date"].iat[-1])
    today = datetime.date.today()
    # delta is not inclusive of today, so add 1 to delta
    delta = today - start_date.date()
    total_elapsed_days = delta.days + 1
    target_days = len(df.index)
    target_days_percent = round(target_days / total_elapsed_days * 100, 1)
    summary = [
        f"Total days : {total_elapsed_days}",
        f"Target days: {target_days} ({target_days_percent}%)",
        # f"Longest streak: x days",
        # f"Current streak: x days",
    ]

    return summary


def results_summary_time(dictionary):
    """
    Scan dictionary of dates and times as key-value pairs and return a summary
    of the data.
    """
    dates_times = dictionary
    # Convert the dictionary str values (hh:mm) to ints (minutes)
    for key, value in dates_times.items():
        dates_times[key] = convert_time_to_minutes(value)
    entries = len(dates_times)
    latest = convert_minutes_to_time(next(iter(dates_times.items()))[1])
    high = convert_minutes_to_time(max(dates_times.values()))
    low = convert_minutes_to_time(min(dates_times.values()))
    mean = convert_minutes_to_time(round(sum(dates_times.values()) / entries))
    summary = [
        f"Latest: {latest[0]}:{latest[1]:02d}"
        f"\t(recorded on {next(iter(dates_times.items()))[0]})",
        f"High  : {high[0]}:{high[1]:02d}"
        f"\t(recorded on {max(dates_times, key=dates_times.get)})",
        f"Low   : {low[0]}:{low[1]:02d}"
        f"\t(recorded on {min(dates_times, key=dates_times.get)})",
        f"Mean  : {mean[0]}:{mean[1]:02d}"
        f"\t(calculated from {entries} items)",
    ]

    return summary


def save_data(dictionary, path):
    """Write dictionary to path in JSON format."""
    content = json.dumps(dictionary)
    save_path = Path(f"{path}.json")
    message = f"Data saved to {save_path}"

    Path(save_path).write_text(content, encoding="utf-8")

    return message
