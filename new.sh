#!/usr/bin/env bash
# 
# new.sh -- by Daniel Wayne Armstrong <hello@dwarmstrong.org>

set -euo pipefail

SCRIPT=$(basename $0)
PURPOSE="Bash shell script template"


Help() {
  echo "usage: $SCRIPT [-h] [-x YYY]"
  echo ""
  echo "$PURPOSE"
  echo ""
  echo "positional arguments:"
  echo "  xxx   yyyy"
  echo ""
  echo "options:"
  echo "  -h    show this help message and exit"
}


run_options() {
  while getopts ":hx:" OPT; do
    case $OPT in
      h)
        Help
        exit
        ;;
      x)
        some_variable="$OPTARG"
        ;;
      ?)
        echo "\"$OPTARG\": Invalid option"
        exit 1
        ;;
    esac
  done
}


load_libraries() {
  # add useful functions
  local lib="lib_homebin.sh"
  local bin="${HOME}/bin/${lib}"
  local src="https://gitlab.com/dwarmstrong/homebin/-/blob/master/${lib}"

  if [ -f $bin ]; then
    source $bin
  else
    echo "error: $bin not found"
    echo "source: $src"
    exit 1
  fi
}


main() {
  echo "main"
}


# (O<  Let's go!
# (/)_
run_options "$@"
load_libraries
main
